package model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the personalmedical database table.
 * 
 */
@Entity
@NamedQuery(name="Personalmedical.findAll", query="SELECT p FROM Personalmedical p")
public class Personalmedical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idPersonalMedical;

	private String nume;

	private String prenume;

	private int salariu;

	private String tip;

	//bi-directional many-to-one association to Diagnostic
	@OneToMany(mappedBy="personalmedical")
	private List<Diagnostic> diagnostics;

	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="personalmedical")
	private List<Programare> programares;

	public Personalmedical() {
		diagnostics = new ArrayList<Diagnostic>();
		programares = new ArrayList<Programare>();
	}

	public int getIdPersonalMedical() {
		return this.idPersonalMedical;
	}

	public void setIdPersonalMedical(int idPersonalMedical) {
		this.idPersonalMedical = idPersonalMedical;
	}

	public String getNume() {
		return this.nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return this.prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public int getSalariu() {
		return this.salariu;
	}

	public void setSalariu(int salariu) {
		this.salariu = salariu;
	}

	public String getTip() {
		return this.tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public List<Diagnostic> getDiagnostics() {
		return this.diagnostics;
	}

	public void setDiagnostics(List<Diagnostic> diagnostics) {
		this.diagnostics = diagnostics;
	}

	public Diagnostic addDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().add(diagnostic);
		diagnostic.setPersonalmedical(this);

		return diagnostic;
	}

	public Diagnostic removeDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().remove(diagnostic);
		diagnostic.setPersonalmedical(null);

		return diagnostic;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setPersonalmedical(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setPersonalmedical(null);

		return programare;
	}

}