package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import model.Diagnostic;
import model.Personalmedical;
import model.Programare;

/**
 * @author k0bra
 * Test class for Personalmedical class
 */
public class PersonalMedicalTests {

	/**
	 * Test method for basic getters and setters
	 */
	@org.junit.Test
	public void basicGettersAndSetters() {
		Personalmedical medic = new Personalmedical();
		String tip = new String("tip"),
				nume = new String("nume"),
				prenume = new String("prenume");
		int salariu = 13,
				idPersonalMedical = 14;
		
		medic.setIdPersonalMedical(idPersonalMedical);
		medic.setNume(nume);
		medic.setPrenume(prenume);
		medic.setSalariu(salariu);
		medic.setTip(tip);
		
		assertTrue(tip.equals(medic.getTip()));
		assertTrue(nume.equals(medic.getNume()));
		assertTrue(prenume.equals(medic.getPrenume()));
		assertTrue(salariu == medic.getSalariu());
		assertTrue(idPersonalMedical == medic.getIdPersonalMedical());
	}

	/**
	 * Test method for appointment list setter
	 */
	@org.junit.Test
	public void programariListSetter() {
		Personalmedical medic = new Personalmedical();
		List<Programare> programariList = new ArrayList<Programare>();
		Programare tempProgramare1 = new Programare();
		Programare tempProgramare2 = new Programare();
		Programare tempProgramare3 = new Programare();
		
		tempProgramare1.setIdProgramare(1);
		programariList.add(tempProgramare1);
		
		tempProgramare2.setIdProgramare(2);
		programariList.add(tempProgramare2);
		
		tempProgramare3.setIdProgramare(3);
		programariList.add(tempProgramare3);
		
		medic.setProgramares(programariList);
		
		List<Programare> newProgramariList = medic.getProgramares();
		
		assertTrue(newProgramariList.size() == programariList.size());
		assertTrue(newProgramariList.get(0).getIdProgramare() == programariList.get(0).getIdProgramare());
		assertTrue(newProgramariList.get(1).getIdProgramare() == programariList.get(1).getIdProgramare());
		assertTrue(newProgramariList.get(2).getIdProgramare() == programariList.get(2).getIdProgramare());
	}
	
	/**
	 * Test method for diagnostic list setter
	 */
	@org.junit.Test
	public void diagnosticListSetter() {
		Personalmedical medic = new Personalmedical();
		List<Diagnostic> diagnosticList = new ArrayList<Diagnostic>();
		Diagnostic tempDiagnostic1 = new Diagnostic();
		Diagnostic tempDiagnostic2 = new Diagnostic();
		Diagnostic tempDiagnostic3 = new Diagnostic();
		
		tempDiagnostic1.setIdDiagnostic(1);
		diagnosticList.add(tempDiagnostic1);
		
		tempDiagnostic2.setIdDiagnostic(2);
		diagnosticList.add(tempDiagnostic2);
		
		tempDiagnostic3.setIdDiagnostic(3);
		diagnosticList.add(tempDiagnostic3);
		
		medic.setDiagnostics(diagnosticList);
		
		List<Diagnostic> newDiagnosticList = medic.getDiagnostics();
		
		assertTrue(newDiagnosticList.size() == diagnosticList.size());
		assertTrue(newDiagnosticList.get(0).getIdDiagnostic() == diagnosticList.get(0).getIdDiagnostic());
		assertTrue(newDiagnosticList.get(1).getIdDiagnostic() == diagnosticList.get(1).getIdDiagnostic());
		assertTrue(newDiagnosticList.get(2).getIdDiagnostic() == diagnosticList.get(2).getIdDiagnostic());
	}	

	/**
	 * Test method for adding elements in the diagnostic list using addDiagnostic
	 */
	@org.junit.Test
	public void diagnosticListAdd() {
		Personalmedical medic = new Personalmedical();
		Diagnostic tempDiagnostic1 = new Diagnostic();
		Diagnostic tempDiagnostic2 = new Diagnostic();
		Diagnostic tempDiagnostic3 = new Diagnostic();
		
		tempDiagnostic1.setIdDiagnostic(1);
		medic.addDiagnostic(tempDiagnostic1);
		
		tempDiagnostic2.setIdDiagnostic(2);
		medic.addDiagnostic(tempDiagnostic2);
		
		tempDiagnostic3.setIdDiagnostic(3);
		medic.addDiagnostic(tempDiagnostic3);
		
		List<Diagnostic> newDiagnosticList = medic.getDiagnostics();
		
		assertTrue(newDiagnosticList.size() == 3);
		assertTrue(newDiagnosticList.get(0).getIdDiagnostic() == 1);
		assertTrue(newDiagnosticList.get(1).getIdDiagnostic() == 2);
		assertTrue(newDiagnosticList.get(2).getIdDiagnostic() == 3);
	}	
	
	/**
	 * Test method for deleting elements from the diagnostic list using removeDiagnostic
	 */
	@org.junit.Test
	public void diagnosticListRemove() {
		Personalmedical medic = new Personalmedical();
		Diagnostic tempDiagnostic1 = new Diagnostic();
		Diagnostic tempDiagnostic2 = new Diagnostic();
		Diagnostic tempDiagnostic3 = new Diagnostic();
		
		tempDiagnostic1.setIdDiagnostic(1);
		medic.addDiagnostic(tempDiagnostic1);
		
		tempDiagnostic2.setIdDiagnostic(2);
		medic.addDiagnostic(tempDiagnostic2);
		
		tempDiagnostic3.setIdDiagnostic(3);
		medic.addDiagnostic(tempDiagnostic3);
		
		medic.removeDiagnostic(tempDiagnostic2);
		
		List<Diagnostic> newDiagnosticList = medic.getDiagnostics();
		
		assertTrue(newDiagnosticList.size() == 2);
		assertTrue(newDiagnosticList.get(1).getIdDiagnostic() == 3);
		
		medic.removeDiagnostic(tempDiagnostic1);
		
		newDiagnosticList = medic.getDiagnostics();
		
		assertTrue(newDiagnosticList.size() == 1);
		assertTrue(newDiagnosticList.get(0).getIdDiagnostic() == 3);
		
		medic.removeDiagnostic(tempDiagnostic3);
		
		newDiagnosticList = medic.getDiagnostics();
		
		assertTrue(newDiagnosticList.size() == 0);

	}	
	
	/**
	 * Test method for adding elements in the appointment list using addProgramare
	 */
	@org.junit.Test
	public void programariListAdd() {
		Personalmedical medic = new Personalmedical();
		Programare tempProgramare1 = new Programare();
		Programare tempProgramare2 = new Programare();
		Programare tempProgramare3 = new Programare();
		
		tempProgramare1.setIdProgramare(1);
		medic.addProgramare(tempProgramare1);
		
		tempProgramare2.setIdProgramare(2);
		medic.addProgramare(tempProgramare2);
		
		tempProgramare3.setIdProgramare(3);
		medic.addProgramare(tempProgramare3);
		
		List<Programare> newProgramariList = medic.getProgramares();
		
		assertTrue(newProgramariList.size() == 3);
		assertTrue(newProgramariList.get(0).getIdProgramare() == 1);
		assertTrue(newProgramariList.get(1).getIdProgramare() == 2);
		assertTrue(newProgramariList.get(2).getIdProgramare() == 3);
	}	
	
	/**
	 * Test method for deleting elements from the appointment list using removeProgramare
	 */
	@org.junit.Test
	public void programariListRemove() {
		Personalmedical medic = new Personalmedical();
		Programare tempProgramare1 = new Programare();
		Programare tempProgramare2 = new Programare();
		Programare tempProgramare3 = new Programare();
		
		tempProgramare1.setIdProgramare(1);
		medic.addProgramare(tempProgramare1);
		
		tempProgramare2.setIdProgramare(2);
		medic.addProgramare(tempProgramare2);
		
		tempProgramare3.setIdProgramare(3);
		medic.addProgramare(tempProgramare3);
		
		medic.removeProgramare(tempProgramare1);
		
		List<Programare> newProgramariList = medic.getProgramares();
		
		assertTrue(newProgramariList.size() == 2);
		assertTrue(newProgramariList.get(0).getIdProgramare() == 2);
		
		medic.removeProgramare(tempProgramare2);
		
		newProgramariList = medic.getProgramares();
		
		assertTrue(newProgramariList.size() == 1);
		assertTrue(newProgramariList.get(0).getIdProgramare() == 3);
		
		medic.removeProgramare(tempProgramare3);
		assertTrue(newProgramariList.size() == 0);
	}	
}
