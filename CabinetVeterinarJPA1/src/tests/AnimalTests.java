package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import model.Animal;
import model.Diagnostic;
import model.Programare;

/**
 * @author k0bra
 * Test class for Animal class
 */
public class AnimalTests {

	/**
	 * Test method for basic getters and setters
	 */
	@org.junit.Test
	public void basicGettersAndSetters() {
		Animal animal = new Animal();
		String email = new String("email"),
				nume = new String("nume"),
				numeOwner = new String("owner"),
				telefonOwner = new String("telefon"),
				specie = new String("specie");	
		int greutate = 13,
			idAnimal = 14;
		
		animal.setEmailOwner(email);
		animal.setNume(nume);
		animal.setNumeOwner(numeOwner);
		animal.setIdAnimal(idAnimal);
		animal.setSpecie(specie);
		animal.setTelefonOwner(telefonOwner);
		animal.setGreutate(greutate);
		
		assertTrue(email.equals(animal.getEmailOwner()));
		assertTrue(nume.equals(animal.getNume()));
		assertTrue(numeOwner.equals(animal.getNumeOwner()));
		assertTrue(telefonOwner.equals(animal.getTelefonOwner()));
		assertTrue(specie.equals(animal.getSpecie()));
		assertTrue(greutate == animal.getGreutate());
		assertTrue(idAnimal == animal.getIdAnimal());
	}

	/**
	 * Test method for appointment list setter
	 */
	@org.junit.Test
	public void programariListSetter() {
		Animal animal = new Animal();
		List<Programare> programariList = new ArrayList<Programare>();
		Programare tempProgramare1 = new Programare();
		Programare tempProgramare2 = new Programare();
		Programare tempProgramare3 = new Programare();
		
		tempProgramare1.setIdProgramare(1);
		programariList.add(tempProgramare1);
		
		tempProgramare2.setIdProgramare(2);
		programariList.add(tempProgramare2);
		
		tempProgramare3.setIdProgramare(3);
		programariList.add(tempProgramare3);
		
		animal.setProgramares(programariList);
		
		List<Programare> newProgramariList = animal.getProgramares();
		
		assertTrue(newProgramariList.size() == programariList.size());
		assertTrue(newProgramariList.get(0).getIdProgramare() == programariList.get(0).getIdProgramare());
		assertTrue(newProgramariList.get(1).getIdProgramare() == programariList.get(1).getIdProgramare());
		assertTrue(newProgramariList.get(2).getIdProgramare() == programariList.get(2).getIdProgramare());
	}
	
	/**
	 * Test method for diagnostic list setter
	 */
	@org.junit.Test
	public void diagnosticListSetter() {
		Animal animal = new Animal();
		List<Diagnostic> diagnosticList = new ArrayList<Diagnostic>();
		Diagnostic tempDiagnostic1 = new Diagnostic();
		Diagnostic tempDiagnostic2 = new Diagnostic();
		Diagnostic tempDiagnostic3 = new Diagnostic();
		
		tempDiagnostic1.setIdDiagnostic(1);
		diagnosticList.add(tempDiagnostic1);
		
		tempDiagnostic2.setIdDiagnostic(2);
		diagnosticList.add(tempDiagnostic2);
		
		tempDiagnostic3.setIdDiagnostic(3);
		diagnosticList.add(tempDiagnostic3);
		
		animal.setDiagnostics(diagnosticList);
		
		List<Diagnostic> newDiagnosticList = animal.getDiagnostics();
		
		assertTrue(newDiagnosticList.size() == diagnosticList.size());
		assertTrue(newDiagnosticList.get(0).getIdDiagnostic() == diagnosticList.get(0).getIdDiagnostic());
		assertTrue(newDiagnosticList.get(1).getIdDiagnostic() == diagnosticList.get(1).getIdDiagnostic());
		assertTrue(newDiagnosticList.get(2).getIdDiagnostic() == diagnosticList.get(2).getIdDiagnostic());
	}	

	/**
	 * Test method for adding elements in the diagnostic list using addDiagnostic
	 */
	@org.junit.Test
	public void diagnosticListAdd() {
		Animal animal = new Animal();
		Diagnostic tempDiagnostic1 = new Diagnostic();
		Diagnostic tempDiagnostic2 = new Diagnostic();
		Diagnostic tempDiagnostic3 = new Diagnostic();
		
		tempDiagnostic1.setIdDiagnostic(1);
		animal.addDiagnostic(tempDiagnostic1);
		
		tempDiagnostic2.setIdDiagnostic(2);
		animal.addDiagnostic(tempDiagnostic2);
		
		tempDiagnostic3.setIdDiagnostic(3);
		animal.addDiagnostic(tempDiagnostic3);
		
		List<Diagnostic> newDiagnosticList = animal.getDiagnostics();
		
		assertTrue(newDiagnosticList.size() == 3);
		assertTrue(newDiagnosticList.get(0).getIdDiagnostic() == 1);
		assertTrue(newDiagnosticList.get(1).getIdDiagnostic() == 2);
		assertTrue(newDiagnosticList.get(2).getIdDiagnostic() == 3);
	}	
	
	/**
	 * Test method for deleting elements from the diagnostic list using removeDiagnostic
	 */
	@org.junit.Test
	public void diagnosticListRemove() {
		Animal animal = new Animal();
		Diagnostic tempDiagnostic1 = new Diagnostic();
		Diagnostic tempDiagnostic2 = new Diagnostic();
		Diagnostic tempDiagnostic3 = new Diagnostic();
		
		tempDiagnostic1.setIdDiagnostic(1);
		animal.addDiagnostic(tempDiagnostic1);
		
		tempDiagnostic2.setIdDiagnostic(2);
		animal.addDiagnostic(tempDiagnostic2);
		
		tempDiagnostic3.setIdDiagnostic(3);
		animal.addDiagnostic(tempDiagnostic3);
		
		animal.removeDiagnostic(tempDiagnostic2);
		
		List<Diagnostic> newDiagnosticList = animal.getDiagnostics();
		
		assertTrue(newDiagnosticList.size() == 2);
		assertTrue(newDiagnosticList.get(1).getIdDiagnostic() == 3);
		
		animal.removeDiagnostic(tempDiagnostic1);
		
		newDiagnosticList = animal.getDiagnostics();
		
		assertTrue(newDiagnosticList.size() == 1);
		assertTrue(newDiagnosticList.get(0).getIdDiagnostic() == 3);
		
		animal.removeDiagnostic(tempDiagnostic3);
		
		newDiagnosticList = animal.getDiagnostics();
		
		assertTrue(newDiagnosticList.size() == 0);

	}	
	
	/**
	 * Test method for adding elements in the appointment list using addProgramare
	 */
	@org.junit.Test
	public void programariListAdd() {
		Animal animal = new Animal();
		Programare tempProgramare1 = new Programare();
		Programare tempProgramare2 = new Programare();
		Programare tempProgramare3 = new Programare();
		
		tempProgramare1.setIdProgramare(1);
		animal.addProgramare(tempProgramare1);
		
		tempProgramare2.setIdProgramare(2);
		animal.addProgramare(tempProgramare2);
		
		tempProgramare3.setIdProgramare(3);
		animal.addProgramare(tempProgramare3);
		
		List<Programare> newProgramariList = animal.getProgramares();
		
		assertTrue(newProgramariList.size() == 3);
		assertTrue(newProgramariList.get(0).getIdProgramare() == 1);
		assertTrue(newProgramariList.get(1).getIdProgramare() == 2);
		assertTrue(newProgramariList.get(2).getIdProgramare() == 3);
	}	
	
	/**
	 * Test method for deleting elements from the appointment list using removeProgramare
	 */
	@org.junit.Test
	public void programariListRemove() {
		Animal animal = new Animal();
		Programare tempProgramare1 = new Programare();
		Programare tempProgramare2 = new Programare();
		Programare tempProgramare3 = new Programare();
		
		tempProgramare1.setIdProgramare(1);
		animal.addProgramare(tempProgramare1);
		
		tempProgramare2.setIdProgramare(2);
		animal.addProgramare(tempProgramare2);
		
		tempProgramare3.setIdProgramare(3);
		animal.addProgramare(tempProgramare3);
		
		animal.removeProgramare(tempProgramare1);
		
		List<Programare> newProgramariList = animal.getProgramares();
		
		assertTrue(newProgramariList.size() == 2);
		assertTrue(newProgramariList.get(0).getIdProgramare() == 2);
		
		animal.removeProgramare(tempProgramare2);
		
		newProgramariList = animal.getProgramares();
		
		assertTrue(newProgramariList.size() == 1);
		assertTrue(newProgramariList.get(0).getIdProgramare() == 3);
		
		animal.removeProgramare(tempProgramare3);
		assertTrue(newProgramariList.size() == 0);
	}	
	
}
