package util;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import model.Animal;
import model.Diagnostic;
import model.Personalmedical;
import model.Programare;

public class DatabaseUtil {
	
	private static DatabaseUtil instance = new DatabaseUtil();
	
	public  EntityManagerFactory entityManagerFactory;
	public  EntityManager entityManager;
	
	/**
	 * Initial setup for the entity manager and the entity manager factory
	 * @throws Exception
	 */
	public void setUp() throws Exception {
		instance.entityManagerFactory = Persistence.createEntityManagerFactory("CabinetVeterinarJPA1");
		instance.entityManager = instance.entityManagerFactory.createEntityManager();
	}
	
	/**
	 * Saves medical personnel entry to database
	 * @param personalMedical new database entry
	 */
	public void savePersonalMedical(Personalmedical personalMedical) {
		instance.entityManager.persist(personalMedical);
	}
	
	/**
	 * Saves appointment entry to database
	 * @param programare new database entry
	 */
	public void saveProgramare(Programare programare) {
		instance.entityManager.persist(programare);
	}
	
	/**
	 * Saves animal entry to database
	 * @param animal new database entry
	 */
	public void saveAnimal(Animal animal) {
		instance.entityManager.persist(animal);
	}
	
	/**
	 * Saves diagnostic entry to database
	 * @param diagnostic new database entry
	 */
	public void saveDiagnostic(Diagnostic diagnostic) {
		instance.entityManager.persist(diagnostic);
	}
	
	/**
	 * Retrieves medical personnel entry from database using its primary key
	 * @param primaryKey the entity's unique identifier
	 */
	public Personalmedical retrievePersonalMedical(int primaryKey) {
		return instance.entityManager.find(Personalmedical.class, primaryKey);
	}
	
	/**
	 * Retrieves appointment entry from database using its primary key
	 * @param primaryKey the entity's unique identifier
	 */
	public Programare retrieveProgramare(int primaryKey) {
		return instance.entityManager.find(Programare.class, primaryKey);
	}
	
	/**
	 * Retrieves diagnostic entry from database using its primary key
	 * @param primaryKey the entity's unique identifier
	 */
	public Diagnostic retrieveDiagnostic(int primaryKey) {
		return instance.entityManager.find(Diagnostic.class, primaryKey);
	}
	
	/**
	 * Retrieves animal entry from database using its primary key
	 * @param primaryKey the entity's unique identifier
	 */
	public Animal retrieveAnimal(int primaryKey) {
		return instance.entityManager.find(Animal.class, primaryKey);
	}
	
	/**
	 * Deletes medical personnel entry from database using the primary key
	 * @param primaryKey the entity's unique identifier
	 */
	public void deletePersonalMedical(int primaryKey) {
		Personalmedical personalMedical = retrievePersonalMedical(primaryKey);
		instance.entityManager.remove(personalMedical);
	}
	
	
	/**
	 * Deletes diagnostic entry from database using the primary key
	 * @param primaryKey the entity's unique identifier
	 */
	public void deleteDiagnostic(int primaryKey) {
		Diagnostic diagnostic = retrieveDiagnostic(primaryKey);
		instance.entityManager.remove(diagnostic);
	}
	
	/**
	 * Deletes appointment entry from database using the primary key
	 * @param primaryKey the entity's unique identifier
	 */
	public void deleteProgramare(int primaryKey) {
		Programare programare = retrieveProgramare(primaryKey);
		instance.entityManager.remove(programare);
	}
	
	/**
	 * Deletes animal entry from database using the primary key
	 * @param primaryKey the entity's unique identifier
	 */
	public void deleteAnimal(int primaryKey) {
		Animal animal = retrieveAnimal(primaryKey);
		instance.entityManager.remove(animal);
	}
	
	
	/**
	 * Starts the transaction between the program and the database
	 */
	public void startTransaction() {
		instance.entityManager.getTransaction().begin();
	}
	
	/**
	 * Finalizes the transaction between the program and the database
	 */
	public void commitTransaction() {
		instance.entityManager.getTransaction().commit();
	}
	
	
	/**
	 * Closes the entity manager
	 */
	public void closeEntityManager() {
		instance.entityManager.close();
	}
	
	
	/**
	 * Prints all appointments from the database sorted by date in ascending order
	 */
	public void printSortedAppointments() {
		List<Programare> results = instance.entityManager.createNativeQuery("Select * from cabinetveterinar.programare order by data", Programare.class)
				.getResultList();
		for(Programare programare : results) {
			System.out.println("Programarea pentru: " + programare.getAnimal().getNume()
			+ " la data si ora de: " + programare.getData()
			+ " cu doctorul: " + programare.getPersonalmedical().getNume()
			+ " " + programare.getPersonalmedical().getPrenume());
		}
	}
	
	/**
	 * Prints all animals found in the database
	 */
	public void printAllAnimalsFromDB() {
		List<Animal> results = instance.entityManager.createNativeQuery("Select * from cabinetveterinar.animal", Animal.class)
				.getResultList();
		for(Animal animal : results) {
			System.out.println(animal.getSpecie() + ": " + animal.getNume() + " has ID: " + animal.getIdAnimal());
		}
	}
	
	/**
	 * Prints all medical personnel found in the database
	 */
	public void printAllMedicalPersonnelFromDB() {
		List<Personalmedical> results = instance.entityManager.createNativeQuery("Select * from cabinetveterinar.personalmedical", Personalmedical.class)
				.getResultList();
		for(Personalmedical personalMedical : results) {
			System.out.println("Prenume: " + personalMedical.getPrenume() + " Nume: " + personalMedical.getNume() 
			+ " has ID: " + personalMedical.getIdPersonalMedical());
		}
	}
	
	/**
	 * Prints all appointments found in the database
	 */
	public void printAllAppointmentsFromDB() {
		List<Programare> results = instance.entityManager.createNativeQuery("Select * from cabinetveterinar.programare", Programare.class)
				.getResultList();
		for(Programare programare : results) {
			System.out.println("Programarea pentru: " + programare.getAnimal().getNume()
					+ " la data de: " + programare.getData()
					+ " cu doctorul: " + programare.getPersonalmedical().getNume()
					+ " " + programare.getPersonalmedical().getPrenume());
		}
	}
	
	/**
	 * Retrieves the appointment list from the database
	 * @return An appointment list
	 */
	public List<Programare> programariList() {
		List<Programare> programariList = (List<Programare>)instance.entityManager.createNativeQuery("Select * from cabinetveterinar.programare order by data DESC, ora ",Programare.class).getResultList();
		return programariList;
	}
	
	/**
	 * Retrieves the diagnostic list from the database
	 * @return A diagnostic list
	 */
	public List<Diagnostic> diagnosticList() {
		List<Diagnostic> diagnosticList = (List<Diagnostic>)instance.entityManager.createNativeQuery("Select * from cabinetveterinar.diagnostic order by idDiagnostic",Diagnostic.class).getResultList();
		return diagnosticList;
	}

	/**
	 * Finds the maximum current value of idDiagnostic from the "diagnostic entity"
	 * @return The highest id value for "diagnostic" entity
	 */
	public int maxDiagnosticID() {
		List<Diagnostic> diagnosticList = diagnosticList();
		return diagnosticList.get(diagnosticList.size() - 1).getIdDiagnostic();
	}
}

