package crud;

import model.Animal;
import model.Personalmedical;
import model.Programare;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;
import util.DatabaseUtil;

public class CRUD {
	
	/**
	 * Manages CRUD operations for Animale class
	 * @param optiune Determines what operation is executed: 1-Create, 2-Retrieve, 3-Update, 4-Delete
	 * @param dbUtil DatabaseUtil class
	 * @throws Exception 
	 */
	public void CRUDAnimale(int optiune, DatabaseUtil dbUtil) throws Exception {
		
		Scanner in = new Scanner(System.in);
		
		switch(optiune) {
		case 1: {
			System.out.println("Specie animal:");
			String specie = in.next();
			System.out.println("Nume animal:");
			String nume = in.next();
			System.out.println("ID animal:");
			int id = in.nextInt();
			Animal animal = new Animal();
			
			animal.setIdAnimal(id);
			animal.setNume(nume);
			animal.setSpecie(specie);
			dbUtil.saveAnimal(animal);
			break;
		}
		case 2:{ 
			System.out.println("Introduceti ID pt gasire:");
			int id = in.nextInt();
			
			// Checks if the entity exists
			if(dbUtil.retrieveAnimal(id) == null)  {
				System.out.println("Nu exista entitatea la care va referiti.");
				in.close();
				return;
			}
			
			Animal animal = dbUtil.retrieveAnimal(id);
			System.out.println(animal.getSpecie() + ": " + animal.getNume() + " has ID: " + animal.getIdAnimal());
			break;
		}
		case 3:{ 
			System.out.println("Introduceti ID pt updatare:");
			int id = in.nextInt();
			
			// Checks if the entity exists
			if(dbUtil.retrieveAnimal(id) == null)  {
				System.out.println("Nu exista entitatea la care va referiti.");
				in.close();
				return;
			}
			
			Animal animal = dbUtil.retrieveAnimal(id);
			
			System.out.println("Specie noua:");
			String specie = in.next();
			System.out.println("Nume nou:");
			String nume = in.next();
			System.out.println("ID nou:");
			int idNew = in.nextInt();
			
			animal.setIdAnimal(idNew);
			animal.setNume(nume);
			animal.setSpecie(specie);

			break;
		}
		case 4:{ 
			System.out.println("Introduceti ID pt stergere:");
			int id = in.nextInt();	
			
			// Checks if the entity exists
			if(dbUtil.retrieveAnimal(id) == null)  {
				System.out.println("Nu exista entitatea la care va referiti.");
				in.close();
				return;
			}
			
			dbUtil.deleteAnimal(id);
			break;
		}
		}
		dbUtil.startTransaction();
		dbUtil.commitTransaction();
		in.close();
	}
	
	/**
	 * Manages CRUD operations for PersonalMedical class
	 * @param optiune Determines what operation is executed: 1-Create, 2-Retrieve, 3-Update, 4-Delete
	 * @param dbUtil DatabaseUtil class
	 *  @throws Exception
	 */
	public void CRUDPersonalMedical(int optiune , DatabaseUtil dbUtil) throws Exception {
		Scanner in = new Scanner(System.in);
		
		switch(optiune) {
		case 1: {
			System.out.println("Prenume: ");
			String prenume = in.next();
			System.out.println("Nume: ");
			String nume = in.next();
			System.out.println("ID: ");
			int id = in.nextInt();
			Personalmedical personalMedical = new Personalmedical();
			
			personalMedical.setIdPersonalMedical(id);
			personalMedical.setNume(nume);
			personalMedical.setPrenume(prenume);
			dbUtil.savePersonalMedical(personalMedical);
			break;
		}
		case 2:{ 
			System.out.println("Introduceti ID pt gasire:");
			int id = in.nextInt();
			
			// Checks if the entity exists
			if(dbUtil.retrievePersonalMedical(id) == null) {
				System.out.println("Nu exista entitatea la care va referiti.");
				in.close();
				return;
			}
			
			Personalmedical personalMedical = dbUtil.retrievePersonalMedical(id);
			System.out.println("Prenume: " + personalMedical.getPrenume() + " Nume: " + personalMedical.getNume() 
			+ " has ID: " + personalMedical.getIdPersonalMedical());
			break;
		}
		case 3:{ 
			System.out.println("Introduceti ID pt updatare:");
			int id = in.nextInt();
			
			// Checks if the entity exists
			if(dbUtil.retrievePersonalMedical(id) == null)  {
				System.out.println("Nu exista entitatea la care va referiti.");
				in.close();
				return;
			}
			
			Personalmedical personalMedical = dbUtil.retrievePersonalMedical(id);
			
			System.out.println("Prenume: ");
			String prenume = in.next();
			System.out.println("Nume: ");
			String nume = in.next();
			System.out.println("ID: ");
			int idNew = in.nextInt();
			
			personalMedical.setIdPersonalMedical(idNew);
			personalMedical.setNume(nume);
			personalMedical.setPrenume(prenume);
			dbUtil.savePersonalMedical(personalMedical);

			break;
		}
		case 4:{ 
			System.out.println("Introduceti ID pt stergere:");
			int id = in.nextInt();	
			
			// Checks if the entity exists
			if(dbUtil.retrievePersonalMedical(id) == null) {
				System.out.println("Nu exista entitatea la care va referiti.");
				in.close();
				return;
			}
			dbUtil.deletePersonalMedical(id);
			break;
		}
		}
		dbUtil.startTransaction();
		dbUtil.commitTransaction();
		in.close();
	}
	
	/**
	 * Manages CRUD operations for Programari class
	 * @param optiune Determines what operation is executed: 1-Create, 2-Retrieve, 3-Update, 4-Delete
	 * @param dbUtil DatabaseUtil class
	 *  @throws Exception
	 */
	public void CRUDProgramari(int optiune, DatabaseUtil dbUtil) throws Exception{
		Scanner in = new Scanner(System.in);

		switch(optiune) {
		case 1: {
			System.out.println("Data(yyyy-mm-dd):");
			String data = in.next();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd", Locale.ENGLISH);
			LocalDate date = LocalDate.parse(data, formatter);
			System.out.println("ID programare:");
			int id = in.nextInt();
			System.out.println("ID animal:");
			int idAnimal = in.nextInt();
			System.out.println("ID personal medical:");
			int idPersonal = in.nextInt();
			
			// Checks if the entities exist
			if((dbUtil.retrieveAnimal(idAnimal) == null) && (dbUtil.retrievePersonalMedical(idPersonal) == null)) {
				System.out.println("Nu exista entitatea la care se refera una din chei starine.");
				in.close();
				return;
			}
			
			Programare programare = new Programare();
			programare.setIdProgramare(id);;
			programare.setAnimal(dbUtil.retrieveAnimal(idAnimal));
			programare.setPersonalmedical(dbUtil.retrievePersonalMedical(idPersonal));
			programare.setData(java.sql.Date.valueOf(date));
			dbUtil.saveProgramare(programare);
			break;
		}
		case 2:{ 
			System.out.println("Introduceti ID pt gasire:");
			int id = in.nextInt();
			
			// Checks if the entity exists
			if(dbUtil.retrieveProgramare(id) == null) {
				System.out.println("Nu exista entitatea la care va referiti.");
				in.close();
				return;
			}
			
			Programare programare = dbUtil.retrieveProgramare(id);
			System.out.println("Programarea pentru: " + programare.getAnimal().getNume()
					+ " la data de: " + programare.getData()
					+ " cu doctorul: " + programare.getPersonalmedical().getNume()
					+ " " + programare.getPersonalmedical().getPrenume());
			break;
		}
		case 3:{ 
			System.out.println("Introduceti ID pt updatare:");
			int id = in.nextInt();
			Programare programare = dbUtil.retrieveProgramare(id);
			
			// Checks if the entity exists
			if(dbUtil.retrieveProgramare(id) == null) {
				System.out.println("Nu exista entitatea la care va referiti.");
				in.close();
				return;
			}
			
			System.out.println("Data(yyyy-mm-dd):");
			String data = in.next();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-dd", Locale.ENGLISH);
			LocalDate date = LocalDate.parse(data, formatter);
			System.out.println("ID programare:");
			int idNew = in.nextInt();
			System.out.println("ID animal:");
			int idAnimal = in.nextInt();
			System.out.println("ID personal medical:");
			int idPersonal = in.nextInt();
			
			if((dbUtil.retrieveAnimal(idAnimal) == null) && (dbUtil.retrievePersonalMedical(idPersonal) == null)) {
				System.out.println("Nu exista entitatea la care se refera una din chei starine.");
				in.close();
				return;
			}
			
			programare.setIdProgramare(idNew);;
			programare.setAnimal(dbUtil.retrieveAnimal(idAnimal));
			programare.setPersonalmedical(dbUtil.retrievePersonalMedical(idPersonal));
			programare.setData(java.sql.Date.valueOf(date));

			break;
		}
		case 4:{ 
			System.out.println("Introduceti ID pt stergere:");
			int id = in.nextInt();	
			
			// Checks if the entity exists
			if(dbUtil.retrieveProgramare(id) == null) {
				System.out.println("Nu exista entitatea la care va referiti.");
				in.close();
				return;
			}
			
			dbUtil.deleteProgramare(id);
			break;
		}
		}
		dbUtil.startTransaction();
		dbUtil.commitTransaction();
		in.close();
	}
	
}
