package view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


/**
 * @author k0bra
 * Controller class for Login screen
 */
public class ControllerLogin{
	
	@FXML
	private PasswordField passwordField;
	@FXML
	private TextField nameField;
	@FXML
	private Label errorText;
	
	/**
	 * Checks if the user introduced data is correct and if so lets them log in
	 */
	@FXML
	private void login() {
		String name = new String(), 
				password = new String(), 
				tempName, tempPassword;
		Boolean first = true;
		
		HashSet<String> data = new HashSet<String>();
		
		data = getDataFromServer();
		
		for(String str: data) {
			if(first) {
				password = str;
				first = false;
			}
			else
				name = str;
		}
		
		tempName = nameField.getText();
		tempPassword = passwordField.getText();
		
		if((tempName.equals(name))&&(tempPassword.equals(password))) {
			 try {
				 Stage stageOld = (Stage) errorText.getScene().getWindow();
				 stageOld.close();
			        
				 FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/View.fxml"));
				 Parent root = (Parent) loader.load();
				 
				 Stage stage = new Stage();			 
				 stage.setScene(new Scene(root));  
				 stage.show();
				 stage.setOnCloseRequest(e -> Platform.exit());
				 
			  	} catch(Exception e) {
			  		e.printStackTrace();
			    }
		} else {
			System.out.println("error");
			errorText.setText("Username and/or Password are incorect!");
		}	
	}	
	
	
	/**
	 * @param username 
	 * @param password
	 * Retrieves the above-mentioned information from the server
	 */
	private HashSet<String> getDataFromServer() {
		HashSet<String> data = new HashSet<String>();
		try {
			String username, password;
			Socket socket = new Socket("localhost", 12222);
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
			output.println("getName");
			username = input.readLine();
			output.println("getPassword");
			password = input.readLine();
			socket.close();
			data.add(username);
			data.add(password);			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}
}
