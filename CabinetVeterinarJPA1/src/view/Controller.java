package view;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Animal;
import model.Diagnostic;
import model.Programare;
import util.DatabaseUtil;


/**
 * @author k0bra
 * Controller class for the main window
 */
public class Controller implements Initializable {

	
	final int MILLIS_IN_SECOND = 1000;
    final int SECONDS_IN_MINUTE = 60;
    final int MINUTES_IN_HOUR = 60;
    final int HOURS_IN_DAY = 24;
    final int DAYS_IN_YEAR = 365; 
    final long MILLISECONDS_IN_YEAR = (long)MILLIS_IN_SECOND * SECONDS_IN_MINUTE * MINUTES_IN_HOUR * HOURS_IN_DAY * DAYS_IN_YEAR;
	
	@FXML
	private ListView<String> listaProgramari;
	@FXML
	private Label nume, varsta, greutate, rasa,
				numeProprietar, telefon, email;
	@FXML
	private Button ok;
	@FXML
	private Button cancel;
	@FXML
	private ListView<String> diagnosticList;
	@FXML
	private ImageView image;
	
	
	private LambdaTwoArg<String, String, Integer> getIDFromListElem = (str1, str2) -> Integer.parseInt(new StringTokenizer(str1).nextToken(str2));
	
	private EventListener onDeleteListener = new EventListener() {
		@Override
		public void writeConsole() {
			System.out.println("Element sters!");
		}
	};
	
	public void setOnDeleteListener(EventListener onDeleteListener) {
		this.onDeleteListener = onDeleteListener;
	}
	
	private EventListener onShowListener = new EventListener() {
		@Override
		public void writeConsole() {
			System.out.println("Element afisat!");
		}
	};
	
	public void setOnShowListener(EventListener onShowListener) {
		this.onShowListener = onShowListener;
	}
	
	
	/**
	 * Populates the list on the left panel, selects the first element if it wasn't selected, and then calls functions to generate the right panel
	 * @throws Exception
	 */
	public void populateMainListView() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction(); 
		
		// Creates the appointment list
		List<Programare> programariDBListInitial = (List<Programare>)db.programariList();
		ObservableList<Programare> programariList = FXCollections.observableArrayList(programariDBListInitial);
		
		// Creates the diagnostic list
		List<Diagnostic> diagnosticeDBListInitial = (List<Diagnostic>)db.diagnosticList();
		ObservableList<Diagnostic> diagnosticeList = FXCollections.observableArrayList(diagnosticeDBListInitial);
		
		// Shows the appointments in list form 
		listaProgramari.setItems(formatProgramareList(programariList));
		listaProgramari.refresh();
		
		// Inserts all of the appointments in a map
		Map<Integer, Programare> programariMap = new HashMap<>();
		for (Programare prog : programariList){
			programariMap.put(prog.getIdProgramare(), prog);
		}
		
		// Attempts to get the currently selected item
		String alfa = listaProgramari.getSelectionModel().getSelectedItem();
		
		// If the attempt failed we select the first element from the list
		if (alfa == null) {
			listaProgramari.scrollTo(0);
			listaProgramari.getSelectionModel().select(0);
			alfa = listaProgramari.getSelectionModel().getSelectedItem();
		}
			
		int id = getIDFromListElem.expression(alfa, ".");
		
		Programare prog = programariMap.get(id);
		
		populateRightPanel(prog.getAnimal());
		
		ObservableList<String> list = formatDiagnosticList(diagnosticeList, prog.getAnimal().getIdAnimal());
		
		if (list != null)
		{
		diagnosticList.setItems(list);
		diagnosticList.refresh();
		}
		
		db.closeEntityManager();
	}
	
	/** 
	 * Populates the right side of the UI
	 * @param animal The animal whose information is shown
	 */
	public void populateRightPanel(Animal animal) {
		
		// Creates the image by converting the byte array obtained from the database into a ByteArrayInputStream
		// Then this becomes the image
		Image photo = new Image(new ByteArrayInputStream(animal.getImage()));
		
		// Calculates the age of the animal
		LambdaOneArg<Animal, Long> calculateAge = (anim) -> 
		(new Date(System.currentTimeMillis()).getTime() - animal.getDataNastere().getTime()) / MILLISECONDS_IN_YEAR;
		// Concatenates a long value and a string
		LambdaTwoArg<Long, String, String> concatenateLongToString = (number, str) -> String.valueOf(number) + str;
		
		varsta.setText(concatenateLongToString.expression(calculateAge.expression(animal), " Ani"));
		greutate.setText(concatenateLongToString.expression(Long.valueOf(animal.getGreutate()), " Kg"));
		
		image.setImage(photo);
		nume.setText(animal.getNume());
		rasa.setText(animal.getSpecie());
		numeProprietar.setText(animal.getNumeOwner());
		email.setText(animal.getEmailOwner());
		telefon.setText(animal.getTelefonOwner());
		onShowListener.writeConsole();
	}
	
	/**
	 * Creates a formatted version of a Diagnostic list
	 * @param diagnostice The diagnostic list that serves as the base list for the formatted list
	 * @param animalID The animal whose previous diagnostics need to be shown
	 * @return A formatted version of a Diagnostic list
	 */
	public ObservableList<String> formatDiagnosticList(List<Diagnostic> diagnostice , int animalID) {
		ObservableList<String> diag = FXCollections.observableArrayList();
		for (Diagnostic diagnostic: diagnostice) {
			if (diagnostic.getAnimal().getIdAnimal() == animalID) {
				diag.add(diagnostic.getTitlu() + " " + diagnostic.getData().toString() + " " + diagnostic.getDescriere());
			}
		}
		return diag;
	}
	
	/**
	 * Creates a formatted version of an appointment list
	 * @param programare The appointment list that serves as the base for the formatted list
	 * @return A formatted version of an appointment list
	 */
	public ObservableList<String> formatProgramareList(List<Programare> programare) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Programare a: programare) {
			names.add(a.getIdProgramare() + ". " + a.getAnimal().getNumeOwner() + " " + a.getOra().toString());
		}
		return names;
	}
	
	/**
	 * Deletes the appointment from the list, after the user has pressed the close button or the ok button
	 * @throws Exception
	 */
	public void close() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		
		String alfa = listaProgramari.getSelectionModel().getSelectedItem();
		int id = getIDFromListElem.expression(alfa, ".");
		
		int removedElementIndex = listaProgramari.getSelectionModel().getSelectedIndex();
		
		listaProgramari.getItems().remove(removedElementIndex);
		db.deleteProgramare(id);
		
		db.commitTransaction();
		db.closeEntityManager();
		
		populateMainListView();
		
		onDeleteListener.writeConsole();
		}
	
	/**
	 * Creates a new window where the user introduces necessary details about the diagnostic, and then waits for that window to close
	 */
	public void ok() {
		// Find element
		String alfa = listaProgramari.getSelectionModel().getSelectedItem();	
		int id = getIDFromListElem.expression(alfa, ".");
		try {	
			 FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DiagnosticInsert.fxml"));
			 Parent root = (Parent) loader.load();
			 Stage stage = new Stage();			 
			 stage.setScene(new Scene(root));  
			 
			 ControllerDiagnostic controller = loader.<ControllerDiagnostic>getController();
			 controller.initialiseData(id);	 
			 
			 stage.showAndWait();
		 
			// Remove selected element from list
			int removedElementIndex = listaProgramari.getSelectionModel().getSelectedIndex();	
			listaProgramari.getItems().remove(removedElementIndex);
			
			populateMainListView();
			
			onDeleteListener.writeConsole();
			
		  	} catch(Exception e) {
		  		e.printStackTrace();
		    }
	}
	
	
	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			populateMainListView();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}

@FunctionalInterface
interface LambdaOneArg<T1, T2> {
	T2 expression(T1 arg);
}

@FunctionalInterface
interface LambdaTwoArg<T1, T2, T3> {
	T3 expression(T1 arg1, T2 arg2);
}
