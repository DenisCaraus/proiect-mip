package view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Diagnostic;
import model.Programare;
import util.DatabaseUtil;

/**
 * @author k0bra
 * Controller class for the DiagnosticInsert window
 */
public class ControllerDiagnostic {
	private static int programareId;
	
	void initialiseData(int id) {
	    programareId = id;
	  }
	
	@FXML
	private TextField titleField;
	@FXML
	private TextArea descriptionField = new TextArea();
	@FXML
	private Label errorText;
	
	/**
	 * Activates when the submit button is pressed
	 * Takes the necessary information and pushes a new diagnostic to the database
	 * @throws Exception
	 */
	@FXML
	private void submit() throws Exception {
		
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		
		Programare currentProgramare = db.retrieveProgramare(programareId);
		
		String title = new String();
		String description = new String();
		
		title = titleField.getText();
		description = descriptionField.textProperty().getValueSafe();
		
		// Validating the input
		if (title.isEmpty()) {
			errorText.setText("Diagnostic title is empty!");
			return;
		}
		if (description.isEmpty()) {
			errorText.setText("Description is empty!");
			return;
		}
		if (description.length() > 250) {
			errorText.setText("Too many characters!");
			return;
		}
		
		Diagnostic newDiagnostic = new Diagnostic();
		newDiagnostic.setAnimal(currentProgramare.getAnimal());
		newDiagnostic.setData(currentProgramare.getData());
		newDiagnostic.setDescriere(description);
		newDiagnostic.setTitlu(title);
		newDiagnostic.setPersonalmedical(currentProgramare.getPersonalmedical());
		newDiagnostic.setIdDiagnostic(db.maxDiagnosticID() + 1);
		
		// Save to database
		
		db.saveDiagnostic(newDiagnostic);
		db.commitTransaction();		
		
		// Delete element from database
		db.startTransaction();
		db.deleteProgramare(programareId);
		db.commitTransaction();
		db.closeEntityManager();
		
		Stage stageOld = (Stage) titleField.getScene().getWindow();
		stageOld.close();
	}	
}
