package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Checker extends Thread{

	private Socket socket;
	
	/**
	 * @param socket
	 * Constructor; Initializes the socket.
	 */
	public Checker(Socket socket) {
		this.socket = socket;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		String name = new String("Admin"), 
		password = new String("password");
		try {	
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
			if(input.readLine().equals("getName"))
				output.println(name);
			if(input.readLine().equals("getPassword"))
				output.println(password);		
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
