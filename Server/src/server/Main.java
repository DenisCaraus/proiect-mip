package server;

import java.io.IOException;
import java.net.ServerSocket;

public class Main {

	public static void main(String[] args) {

		ServerSocket serverSocket;
		
		try {
		serverSocket = new ServerSocket(12222);
		while (true) {
			new Checker(serverSocket.accept()).start();
		}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

}
